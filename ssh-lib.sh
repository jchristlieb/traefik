#!/bin/bash

SSH_PRESERVE_CI_ENV=1
SSH_PRESERVE_ENV=${SSH_PRESERVE_ENV:-}

ssh:keyfile() {
    DEPLOY_SSH_KEY_PATH="${HOME}/.ssh"
    if [ ! -d "$DEPLOY_SSH_KEY_PATH" ]
    then
        mkdir -p "$DEPLOY_SSH_KEY_PATH"
        chmod 700 "$DEPLOY_SSH_KEY_PATH"
    fi
    echo "$DEPLOY_SSH_KEY_PATH/id_rsa_$CI_COMMIT_SHA"
}

ssh:options() {
    DEPLOY_SSH_KEY_FILENAME="$( ssh:keyfile )"
    echo "-o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no -i $DEPLOY_SSH_KEY_FILENAME"
}

ssh:options:login() {
    echo "$DEPLOY_SSH_USERNAME@$DEPLOY_SSH_HOST"
}

ssh:run:raw() {
    ssh -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no -i "$DEPLOY_SSH_KEY_FILENAME" \
        "$( ssh:options:login )" -- "$@"
}

ssh:login() {
    DEPLOY_SSH_KEY_FILENAME="$( ssh:keyfile )"
    (
        # Using subshell, so "set -x" can be skipped and DOCKER_TLS values will not
        # show in CI logs. The "set +x" disables command output, the original
        # setting will be restored at the end of the subshell.
        set +x

        echo "$DEPLOY_SSH_KEY" > "$DEPLOY_SSH_KEY_FILENAME"
        chmod 600 "$DEPLOY_SSH_KEY_FILENAME"
    )
    # test login, to be sure
    ssh:run:raw uname -a
}

ssh:prepare() {
    # Copy docker run script to remote host, so we can preserve some of the ENV vars
    TEMPFILE=$( mktemp )
    TEMPNAME=$( basename "$TEMPFILE" )
    (
        # Using subshell, so "set -x" can be skipped and DOCKER_TLS values will not
        # show in CI logs. The "set +x" disables command output, the original
        # setting will be restored at the end of the subshell.
        set +x

        echo "#!/bin/bash" > "$TEMPFILE"
        # preserve ENV vars
        if [ ! -z "$SSH_PRESERVE_CI_ENV" ]
        then
            for preserve_env in $( compgen -A variable | egrep '^CI_' )
            do
                echo "export $preserve_env=\"${!preserve_env//\"/\\\"}\"" >> "$TEMPFILE"
            done
        fi
        if [ ! -z "$SSH_PRESERVE_ENV" ]
        then
            for preserve_env in $SSH_PRESERVE_ENV
            do
                echo "export $preserve_env=\"${!preserve_env//\"/\\\"}\"" >> "$TEMPFILE"
            done
        fi
        echo '"$@"' >> "$TEMPFILE"
        chmod +x "$TEMPFILE"
        scp $( ssh:options ) "$TEMPFILE" "$( ssh:options:login ):$CI_COMMIT_SHORT_SHA-$CI_JOB_ID-run.sh"
        ssh:run:raw chmod +x "$CI_COMMIT_SHORT_SHA-$CI_JOB_ID-run.sh"
    )
    rm -f "$TEMPFILE"
}

ssh:finish() {
    ssh:run:raw rm -f "$CI_COMMIT_SHORT_SHA-$CI_JOB_ID-run.sh"
}

ssh:run() {
    ssh:run:raw "./$CI_COMMIT_SHORT_SHA-$CI_JOB_ID-run.sh" "$@"
}

ssh:copy() {
    # Copy docker-stack.yml to remote host
    scp $( ssh:options ) "$1" \
        "$( ssh:options:login ):$2"
}

ssh:docker() {
    ssh:run docker "$@"
}

ssh:docker:login() {
    ssh:docker login -u gitlab-ci-token -p "$CI_JOB_TOKEN" "$CI_REGISTRY"
}
